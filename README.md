# media-server
An all-in-one package of services for your media server.

## What's included?
* <img src='http://www.google.com/s2/favicons?domain=plex.tv' height='16' width='16' /> [Plex](https://plex.tv)
* <img src='http://www.google.com/s2/favicons?domain=tautulli.com' height='16' width='16' /> [Tautulli](http://tautulli.com/)
* <img src='http://www.google.com/s2/favicons?domain=radarr.video' height='16' width='16' /> [Radarr](https://radarr.video)
* <img src='http://www.google.com/s2/favicons?domain=sonarr.tv' height='16' width='16' /> [Sonarr](https://sonarr.tv/)
* <img src='https://raw.githubusercontent.com/theotherp/nzbhydra2/master/core/ui-src/img/favicon.ico' height='16' width='16' /> [NZBHydra](https://github.com/theotherp/nzbhydra2)
* <img src='http://www.google.com/s2/favicons?domain=sabnzbd.org' height='16' width='16' /> [SABnzbd](https://sabnzbd.org)
* <img src='https://raw.githubusercontent.com/Jackett/Jackett/9215ae44ba6cc52080bc21142ca56de8319c2e81/src/Jackett.Common/Content/favicon.ico' height='16' width='16' /> [Jackett](https://github.com/Jackett/Jackett)
* <img src='http://www.google.com/s2/favicons?domain=deluge-torrent.org' height='16' width='16' /> [Deluge](https://deluge-torrent.org)
* <img src='http://www.google.com/s2/favicons?domain=heimdall.site' height='16' width='16' /> [Heimdall](https://heimdall.site)
* <img src='http://www.google.com/s2/favicons?domain=openvpn.net' height='16' width='16' /> [OpenVPN](https://openvpn.net/)
* <img src='http://www.google.com/s2/favicons?domain=traefik.io' height='16' width='16' /> [Traefik](https://traefik.io/)
* [ddclient](https://sourceforge.net/p/ddclient/wiki/Home/)

## Getting Started

### Setup
* `npm install`
* `npm docker:build` This will generate the `docker-compose.yml`-file
* Copy the `.env.example` to `.env` and fill in the environment variables.

#### Environment Variables
There are a couple environment variables that you need to fill in.
These variables are discussed in this paragraph

##### General Configuration
>GENERAL_UID

The user ID used inside the Docker containers.
To prevent Docker being run as the root user, you
should fill in your user ID here.

This can be found by executing `id` in a new bash window.
You will get something like this:
```bash
uid=1337(my-user) gid=985(users) groups=985(users),972(docker),998(wheel)
```

So in this case is the user ID 1337.

>GENERAL_GID

Just like the user ID, you also need to fill in the correct
group ID. Just run `id` in a new bash window and you'll get
something like this:
```bash
uid=1337(my-user) gid=985(users) groups=985(users),972(docker),998(wheel)
```

So in this case is the group ID 972.

>GENERAL_UMASK

This setup also allow you to change the umask.
The default is 0022, but you can set this to 0000 if you want.

>GENERAL_TIMEZONE

The timezone that you want to use.
For example `Europe/Amsterdam`. 

##### Directories
>MEDIA_MOVIES_DIR

Point this variable to the directory where all your 
movies are stored.
Can be a network share, mounted in your host machine.

>MEDIA_MUSIC_DIR

Point this variable to the directory where all your 
music is stored.
Can be a network share, mounted in your host machine.

>MEDIA_PHOTOS_DIR

Point this variable to the directory where all your 
photos are stored.
Can be a network share, mounted in your host machine.

>MEDIA_SERIES_DIR

Point this variable to the directory where all your 
series/TV shows are stored.
Can be a network share, mounted in your host machine.

>TMP_DIR

Point this variable to the directory where all your 
temporary files are stored.
Make sure that this directory is readable and writable
by the user ID and group ID you've filled in!

>DATA_DIR

Point this variable to the directory where all the 
data of the containers are stored.
Make sure that this directory is readable and writable
by the user ID and group ID you've filled in!

>LOG_DIR

Point this variable to the directory where all the 
logs of the containers are stored.
Make sure that this directory is readable and writable
by the user ID and group ID you've filled in!

>CONFIG_DIR

Point this variable to the directory where all the 
configurations of the containers are stored.
Make sure that this directory is readable and writable
by the user ID and group ID you've filled in!

>DOWNLOAD_DIR

Point this variable to the directory where all the 
complete downloads should be placed.
Can be a network share, mounted in your host machine.

>INCOMPLETE_DOWNLOAD_DIR

Point this variable to the directory where all the 
incomplete downloads should be placed.
Make sure that this directory is readable and writable
by the user ID and group ID you've filled in!

##### Deluge configuration
>DELUGE_HOST

The hostname for Deluge.
For example `deluge.my-site.com`

##### Heimdall configuration
> HEIMDALL_HOST

The hostname for Heimdall.
For example `heimdall.my-site.com`

##### Jackett configuration
>JACKETT_HOST

The hostname for Jackett.
For example `jackett.my-site.com`

##### NZBHydra configuration
>NZBHYDRA_HOST

The hostname for NZBHydra.
For example `nzbhydra.my-site.com`

##### Plex configuration
>PLEX_VERSION

Which version of Plex should be used.
The default is `latest`.

>PLEX_ADVERTISE_IP

Which IP address or host plex is running on.
Can be different than `PLEX_HOST`.

>PLEX_CLAIM

The Plex claim token.
Can be obtained here `https://plex.tv/claim`

>PLEX_HOST

The hostname for Plex.
For example `plex.my-site.com`


##### Radarr configuration
>RADARR_HOST

The hostname for Radarr.
For example `radarr.my-site.com`

##### SABnzbd configuration
>SABNZBD_HOST

The hostname for SABnzbd.
For example `sabnzbd.my-site.com`

##### Sonarr configuration
>SONARR_HOST

The hostname for Sonarr.
For example `sonarr.my-site.com`

##### Tautulli configuration
>TAUTULLI_HOST

The hostname for Tautulli.
For example `tautulli.my-site.com`

### Go!
* Run `docker-compose up -d` and you're good to go!

### Configuration
You still need to configure ddclient, OpenVPN and Traefik before you can connect to the services.

#### ddclient (Optional)
Go into the `CONFIG_DIR/ddclient` and change the `ddclient.conf`-file to your preferences.

#### OpenVPN
Go into the `CONFIG_DIR/openvpn` and add your OpenVPN configuration here.
This is highly recommended for using Deluge and Jackett.

#### Traefik
Go into the `CONFIG_DIR/traefik` and create the following files:

- `acme.json`
- `.htpasswd`
- `traefik.toml` - Grab the default configuration here: [traefik.sample.toml](https://github.com/containous/traefik/blob/master/traefik.sample.toml)
Make sure That you enable the `docker`-section!
