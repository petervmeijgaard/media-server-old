var path = require('path');
var fs = require('fs');

module.exports = {
    /**
     * Function which will strip the file extension
     *
     * @param {string} fileName The given filename.
     *
     * @returns {string | void} The name of the file without extension.
     */
    stripFileExtension: function (fileName) {
        return fileName.replace(/\.[^/.]+$/, "");
    },

    /**
     * Function which will include the config files of a
     * given type and build an object out of them.
     *
     * @param {string} type The type.
     *
     * @returns {Object} The object filled with the data for the type.
     */
    registerType: function (type) {
        var obj = {};
        var normalizedPath = path.join(__dirname, '../config/' + type);

        fs.readdirSync(normalizedPath)
            .forEach(function (fileName) {
                var name = this.stripFileExtension(fileName);

                obj[name] = require('./../config/' + type + '/' + fileName);
            }.bind(this));

        return obj;
    }
};