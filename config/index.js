var helpers = require('./../utils/helpers.js');

module.exports = {
    version: '3',
    networks: helpers.registerType('networks'),
    services: helpers.registerType('services')
};
